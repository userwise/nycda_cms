class AddTuitionPaidToStudents < ActiveRecord::Migration
  def change
    add_column :students, :tuition_paid, :boolean, default: false
  end
end
