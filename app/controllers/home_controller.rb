class HomeController < ApplicationController
  def show
  end

  def search
    query = params[:query]
    @student = Student.find_by_email(query)

    if @student 
      flash[:notice] = "Student found"
    else
      flash[:alert] = "No Student found"
    end
  end
end
