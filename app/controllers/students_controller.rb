class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def new
    @student = Student.new
  end

  def edit
    @student = Student.find_by_id(params[:id])
  end

  def update
    student = Student.find_by_id(params[:id])

    if student.update_attributes(params[:student])
      # student was updated successfully
      flash[:notice] = "Student was update successfully!"
      redirect_to student_path(student)
    else
      # student was not updated show error message
    end
  end

  def create
    @student = Student.new(params[:student])

    if @student.save
      # student was saved successfully
      flash[:notice] = "Student was saved successfully!"
      redirect_to action: "index"
    else
      # student was not saved due to an error
    end
  end

  def show
    # Example request: /students/3
    # get the id from the params hash
    # and then use find_by_id to look for student with that id
    @student = Student.find_by_id(params[:id])
  end

  def destroy
    # find the id of student we want to delete
    @student = Student.find_by_id(params[:id])
    @student.destroy
    # display a flash message in index view
    flash[:notice] = "Student was destroyed"
    redirect_to students_path
  end
end
