Rails.application.routes.draw do
  get 'home/show'
  get '/search', to: "home#search"

  resources :students
  root 'home#show'
end
